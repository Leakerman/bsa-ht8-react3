const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middleware/user.validation.middleware');
const { responseMiddleware } = require('../middleware/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        res.data = UserService.getAll()
    } catch (err) {
        res.err = err;
        res.code = 404
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        res.data = UserService.search({id:req.params.id})
    } catch (err) {
        res.err = err;
        res.code = 404
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    try {
        if(!res.err) res.data = UserService.create(req.body)
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        if(!res.err) res.data = UserService.update(req.params.id, req.body)
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        res.data = UserService.delete(req.params.id)
    } catch (err) {
        res.err = err;
        res.code = 404
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;