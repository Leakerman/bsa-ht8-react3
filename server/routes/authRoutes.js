const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middleware/response.middleware");
const { loginUserValid } = require("../middleware/user.validation.middleware");

const router = Router();

router.post("/login", loginUserValid, (req, res, next) => {
    try {
      if(!res.err) res.data = AuthService.login(req.body);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;
