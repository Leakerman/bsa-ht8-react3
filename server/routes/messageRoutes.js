const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middleware/response.middleware');
const { createMessageValid, updateMessageValid } = require('../middleware/message.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        res.data = MessageService.getAll()
    } catch (err) {
        res.err = err;
        res.code = 404
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        res.data = MessageService.search({id:req.params.id})
    } catch (err) {
        res.err = err;
        res.code = 404
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createMessageValid, (req, res, next) => {
    try {
        if(!res.err) res.data = MessageService.create(req.body)
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateMessageValid, (req, res, next) => {
    try {
        if(!res.err) res.data = MessageService.update(req.params.id, req.body)
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        res.data = MessageService.delete(req.params.id)
    } catch (err) {
        res.err = err;
        res.code = 404
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;