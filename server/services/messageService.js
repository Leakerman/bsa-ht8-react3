const { MessageRepository } = require("../repositories/messageRepository");

class MessageService {
  search(search) {
    const item = MessageRepository.getOne(search);
    if(!item) {
      throw Error("Messages not found")
    }
    return item 
  }

  getAll() {
    const messages = MessageRepository.getAll();

    if(!messages.length) {
      throw Error("Messages not found")
    }
    return messages;
  }

  create(message) {
    const isCreated = MessageRepository.create(message);
    if (!isCreated) {
      throw Error("Message was not created");
    }
    return isCreated;
  }

  update(id, newMessage) {
    if(this.search({id})) {
      return MessageRepository.update(id, newMessage)
    } else {
      throw Error("Wrong Message id");
    }
  }

  delete(id) {
    const deletedItems = MessageRepository.delete(id);
    if (!deletedItems.length) {
      throw Error("Wrong Message id");
    }
    return deletedItems;
  }
}

module.exports = new MessageService();
