const { user } = require("../models/user");

const createUserValid = (req, res, next) => {
  try {
    const userCandidate = req.body;
    const { password } = userCandidate;

    if (!req.body.avatar) {
      req.body.avatar =
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRX9GqqDvtUhZhJyfKO0Jvv_qxnk1YoXhDdeLBLMq_FzfdGOd2j&s";
    }

    hasExtraProperties(userCandidate, user);
    hasAllProperties(userCandidate, user);
    passwordCheck(password);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  try {
    const userCandidate = req.body;
    const {  password } = userCandidate;

    hasExtraProperties(userCandidate, user);
    if (password) passwordCheck(password);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const loginUserValid = (req, res, next) => {
  try {
    hasAllProperties(req.body, user);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

function hasExtraProperties(user, model) {
  for (let key in user) {
    if (!model.hasOwnProperty(key) || key === "id") {
      throw Error("User body has an extra fields");
    }
  }
}

function hasAllProperties(user, model) {
  const missingFields = []
  const unnecessaryFields = ["id","avatar"]
  const isValid = Object.keys(model).reduce((result, key) => {
    if ( unnecessaryFields.includes(key) ) {
      return result
    } else  {
      if(!user.hasOwnProperty(key)) missingFields.push(key)
      return result && user.hasOwnProperty(key)
    }
  }, true);

  if (!isValid) throw Error(`User body is missing fields: ${missingFields.join(", ")}`);
}

function passwordCheck(password) {
  if (password.length <= 3)
    throw Error("Password must have mor than 3 symbols");
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.loginUserValid = loginUserValid;
