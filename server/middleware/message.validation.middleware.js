const { message } = require("../models/message");

const createMessageValid = (req, res, next) => {
  try {
    const messageCandidate = req.body;
    hasExtraProperties(messageCandidate, message);
    hasAllProperties(messageCandidate, message);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateMessageValid = (req, res, next) => {
  try {
    const messageCandidate = req.body;
    hasExtraProperties(messageCandidate, message);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

function hasExtraProperties(fighter, model) {
  const unnecessaryFields = ["id"]

  for (let key in fighter) {
    if (!model.hasOwnProperty(key) || unnecessaryFields.includes(key)) {
      throw Error("Fighter body has an extra fields");
    }
  }
}

function hasAllProperties(message, model) {
  const missingFields = []
  const unnecessaryFields = ["id","likes"]
  const isValid = Object.keys(model).reduce((result, key) => {
    if ( unnecessaryFields.includes(key) ) {
      return result
    } else  {
      if(!message.hasOwnProperty(key)) missingFields.push(key)
      return result && message.hasOwnProperty(key)
    }
  }, true);

  if (!isValid) throw Error(`Message body is missing fields: ${missingFields.join(", ")}`);
}

exports.createMessageValid = createMessageValid;
exports.updateMessageValid = updateMessageValid;
