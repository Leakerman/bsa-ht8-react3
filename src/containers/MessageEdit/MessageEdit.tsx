import React, { Component } from "react";
import "./MessageEdit.scss";
import { RootState } from "../../store/reducers/rootReducer";
import { connect } from "react-redux";
import { editMessage } from "../../store/actions/chat";

function mapStateToProps(state: RootState) {
  return {
    text: state.chat.editingMessageText,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    editMessage: (messageId: string, text: string) =>
      dispatch(editMessage(messageId, text)),
  };
}

const connector = connect(mapStateToProps, mapDispatchToProps);

interface EditModalState {
  messageId: string;
  text: string;
}

class EditModal extends Component<any, EditModalState> {
  constructor(props: any) {
    super(props);
    this.state = {
      messageId: props.match.params.id,
      text: props.text,
    };
  }

  onEdit() {
    const { messageId, text } = this.state;
    this.props.editMessage(messageId, text);
    this.props.history.push("/chat")
  }

  render() {
    return (
      <div className="container">
        <h4 className="title">Edit message form</h4>

        <input
          onChange={(e) => this.setState({ text: e.target.value })}
          value={this.state.text}
          className="input"
          type="text"
          placeholder="Enter the message"
        />
        <button onClick={this.onEdit.bind(this)} className="button">
          Edit
        </button>
      </div>
    );
  }
}

export default connector(EditModal);
