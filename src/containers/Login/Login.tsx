import { connect, ConnectedProps } from "react-redux";
import React, { Component } from "react";
import axios from "axios";
import { IUser } from "../../types/interfaces";
import { login } from "../../store/actions/auth";
import "./Login.scss";

function mapDispatchToProps(dispatch: any) {
  return {
    login: (user: IUser, isAdmin: boolean) => dispatch(login(user, isAdmin)),
  };
}

const connector = connect(null, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

type LoginState = {
  login: string;
  password: string;
};

class Login extends Component<PropsFromRedux, LoginState> {
  constructor(props: PropsFromRedux) {
    super(props);
    this.onLogin = this.onLogin.bind(this);
    this.state = {
      login: "",
      password: "",
    };
  }

  isAdmin(login: string, password: string): boolean {
    return login === "admin" && password === "admin";
  }

  onLogin() {
    axios
      .post("http://localhost:3050/api/auth/login", {
        login: this.state.login,
        password: this.state.password,
      })
      .then((response) => {
        const { id, login, password, avatar } = response.data;
        const isAdmin = this.isAdmin(login, password);
        const user: IUser = {
          id,
          login,
          avatar,
        };
        this.props.login(user, isAdmin);
      })
      .catch((e) => alert(e.message));
  }

  render() {
    return (
      <div className="login">
        <input
          type="text"
          onChange={(e) => this.setState({ login: e.target.value })}
          placeholder="Login"
        />
        <input
          type="password"
          onChange={(e) => this.setState({ password: e.target.value })}
          placeholder="Password"
        />
        <button onClick={this.onLogin}>Login</button>
      </div>
    );
  }
}

export default connector(Login);
