import React, { Component } from "react";
import "./UserEditor.scss";
import { RootState } from "../../store/reducers/rootReducer";
import { connect } from "react-redux";
import { IUser } from "../../types/interfaces";
import { addUser, updateUser } from "../../store/actions/users";

function mapDispatchToProps(dispatch: any) {
  return {
    addUser: (user: IUser) => dispatch(addUser(user)),
    updateUser: (userId: string, updated: any) =>
      dispatch(updateUser(userId, updated)),
  };
}

function mapStateToProps(state: RootState) {
  return {
    user: state.users.editingUser,
  };
}

const connector = connect(mapStateToProps, mapDispatchToProps);

interface UserEditorState {
  userId: string;
  avatar: string;
  login: string;
  password: string;
}

class UserEditor extends Component<any, UserEditorState> {
  constructor(props: any) {
    super(props);
    this.state = {
      userId: props.match.params.id,
      avatar: props.user.avatar,
      login: props.user.login,
      password: props.user.password,
    };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const { userId, avatar, login, password } = this.state;
    if (!avatar || !login || password.length <=3 ) {
      alert("Fields could not be empty!\n And password must be longer than 3 symbols!");
      return;
    }
    const user: IUser = { avatar, login, password };
    if (userId) {
      this.props.updateUser(userId, user);
    } else {
      this.props.addUser(user);
    }
    this.props.history.push("/users");
  }

  render() {
    return (
      <div className="container">
        <h4 className="title">User editor</h4>

        <input
          onChange={(e) => this.setState({ avatar: e.target.value })}
          value={this.state.avatar}
          className="input"
          type="text"
          placeholder="Enter an avatar url"
        />
        <input
          onChange={(e) => this.setState({ login: e.target.value })}
          value={this.state.login}
          className="input"
          type="text"
          placeholder="Enter a login"
        />
        <input
          onChange={(e) => this.setState({ password: e.target.value })}
          value={this.state.password}
          className="input"
          type="text"
          placeholder="Enter a password"
        />
        <button onClick={this.onClick} className="button">
          {!!this.state.userId ? "Edit" : "Create"}
        </button>
      </div>
    );
  }
}

export default connector(UserEditor);
