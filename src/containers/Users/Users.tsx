import React, { Component } from "react";
import "./Users.scss";
import { RootState } from "../../store/reducers/rootReducer";
import { connect } from "react-redux";
import { IUser } from "../../types/interfaces";
import {
  fetchUsers,
  deleteUser,
  setEditingUser,
} from "../../store/actions/users";

const EMPTY_USER = {
  avatar: "",
  login: "",
  password: "",
};

function mapDispatchToProps(dispatch: any) {
  return {
    fetchUsers: () => dispatch(fetchUsers()),
    deleteUser: (userId: string) => dispatch(deleteUser(userId)),
    setEditingUser: (user: IUser) => dispatch(setEditingUser(user)),
  };
}

function mapStateToProps(state: RootState) {
  return {
    users: state.users.users,
  };
}

const connector = connect(mapStateToProps, mapDispatchToProps);

class Users extends Component<any> {
  componentDidMount() {
    this.props.fetchUsers();
  }

  render() {
    return (
      <div className="wrapper">
        <button
          className="add-user"
          onClick={() => {
            this.props.setEditingUser(EMPTY_USER);
            this.props.history.push(`/users/edit`);
          }}
        >
          Add user
        </button>
        {this.props.users.length ? (
          this.props.users.map((user: IUser) => (
            <div key={user.id} className="user">
              <div className="user__info">{user.login}</div>
              <button
                className="user__edit"
                onClick={() => {
                  this.props.setEditingUser(user);
                  this.props.history.push(`/users/edit/${user.id}`);
                }}
              >
                Edit
              </button>
              <button
                className="user__delete"
                onClick={() => {
                  const response = prompt(`Enter "DELETE" to delete ${user.login}`)
                  if(response === "DELETE") {
                    this.props.deleteUser(user.id!);
                  } 
                }}
              >
                Delete
              </button>
            </div>
          ))
        ) : (
          <h1>There is no users</h1>
        )}
      </div>
    );
  }
}

export default connector(Users);
