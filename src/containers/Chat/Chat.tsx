import React, { Component } from "react";
import { connect } from "react-redux";
import ChatHeader from "../../components/ChatHeader/ChatHeader";
import ChatMessages from "../../components/ChatMessages/ChatMessages";
import Loader from "../../components/Loader/Loader";
import { fetchMessages, sendMessage, setEditingMessageText } from "../../store/actions/chat";
import { IMessage } from "../../types/interfaces";
import { RootState } from "../../store/reducers/rootReducer";
import "./Chat.scss";


function mapStateToProps(state: RootState) {
  return {
    currentUser: state.auth.currentUser,
    messages: state.chat.messages,
    pending: state.chat.pending,
    error: state.chat.error,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    setEditingMessageText: (text: string) => dispatch(setEditingMessageText(text)),
    fetchMessages: () => dispatch(fetchMessages()),
    sendMessage: (message: IMessage) => dispatch(sendMessage(message)),
  };
}

const connector = connect(mapStateToProps, mapDispatchToProps);

class Chat extends Component<any> {
  constructor(props: any) {
    super(props);
    this.onKeyArrowUp = this.onKeyArrowUp.bind(this);
  }

  componentDidMount() {
    this.props.fetchMessages();
    document.addEventListener("keyup", this.onKeyArrowUp);
  }

  componentWillUnmount() {
    document.removeEventListener("keyup", this.onKeyArrowUp);
  }

  onKeyArrowUp(e: KeyboardEvent) {
    if (e.key !== "ArrowUp") return;
    const currUserMessages = this.props.messages.filter(
      (m: IMessage) => m.userId === this.props.currentUser.id
    );
    if (!currUserMessages.length) return;
    const { id: lastMessageId, text } = currUserMessages[currUserMessages.length - 1];
    this.props.setEditingMessageText(text)
    this.props.history.push(`/message/edit/${lastMessageId}`);
  }

  state = {
    messageInputValue: "",
  };

  onMessageInputValueChange(event: React.ChangeEvent<HTMLInputElement>) {
    const value: string = event.currentTarget.value;
    this.setState({ messageInputValue: value });
  }

  onMessageSend() {
    if (!this.state.messageInputValue.trim()) return;
    const { id: userId, login: user, avatar } = this.props.currentUser;

    const message: IMessage = {
      text: this.state.messageInputValue,
      user,
      avatar,
      userId,
      likes: [],
    };

    this.props.sendMessage(message);

    this.setState({ messageInputValue: "" });

    const chatBox = document.querySelector(".chat__messages-box");
    chatBox!.scrollTop = chatBox!.scrollHeight;
    this.focusOnInput();
  }

  focusOnInput() {
    document.getElementById("message-input")?.focus();
  }

  render() {
    return this.props.pending ? (
      <Loader />
    ) : this.props.error ? (
      "An fetch error occured"
    ) : (
      <div className="chat">
        <ChatHeader />
        <ChatMessages />
        <div className="chat__message-form">
          <input
            id="message-input"
            onChange={this.onMessageInputValueChange.bind(this)}
            value={this.state.messageInputValue}
            className="input"
            type="text"
            placeholder="Enter the message"
          />
          <button
            id="send-message-button"
            onClick={this.onMessageSend.bind(this)}
            className="button"
          >
            Send
          </button>
        </div>
      </div>
    );
  }
}

export default connector(Chat);
