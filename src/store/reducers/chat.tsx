import {
  CHAT_SET_EDITING_MESSAGE_TEXT,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_START,
  FETCH_MESSAGES_ERROR,
  ChatActionTypes,
} from "../../types/chat";
import { ChatState } from "../../types/interfaces";

const initialState: ChatState = {
  headerData: {
    chatName: "My chat",
    participants: 0,
    messagesAmount: 0,
    lastMessageDate: "",
  },
  pending: false,
  messages: [],
  messageInputValue: "",
  error: "",
  editingMessageText: "",
};

export default function chatReducer(
  state = initialState,
  action: ChatActionTypes
): ChatState {
  switch (action.type) {
    case CHAT_SET_EDITING_MESSAGE_TEXT:
      return {
        ...state,
        editingMessageText: action.text,
      };
    case FETCH_MESSAGES_START:
      return {
        ...state,
        pending: true,
      };
    case FETCH_MESSAGES_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error,
      };
    case FETCH_MESSAGES_SUCCESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
      };
    default:
      return state;
  }
}
