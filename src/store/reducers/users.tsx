import {
  UsersActionTypes,
  FETCH_USERS_SUCCESS,
  SET_EDITING_USER,
} from "../../types/users";
import { usersState } from "../../types/interfaces";

const initialState: usersState = {
  users: [],
  editingUser: {
    avatar: "",
    login: "",
    password: "",
  },
};

export default function usersReducer(
  state: usersState = initialState,
  action: UsersActionTypes
): usersState {
  switch (action.type) {
    case SET_EDITING_USER: {
      return {
        ...state,
        editingUser: action.user,
      };
    }
    case FETCH_USERS_SUCCESS: {
      return {
        ...state,
        users: [...action.users],
      };
    }
    default:
      return state;
  }
}
