import { AUTH_LOGIN, AuthActionTypes } from "../../types/auth";
import { AuthState } from "../../types/interfaces";

const initialState: AuthState = {
  currentUser: {
    id: "",
    login: "",
    avatar: "",
  },
  isAdmin: false,
  isAuthenticated: false,
};

export default function authReducer(
  state = initialState,
  action: AuthActionTypes
): AuthState {
  switch (action.type) {
    case AUTH_LOGIN: {
      return {
        ...state,
        currentUser: action.user,
        isAdmin: action.isAdmin,
        isAuthenticated: true,
      };
    }
    default:
      return state;
  }
}
