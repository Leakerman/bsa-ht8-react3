import {combineReducers } from 'redux'
import chatReducer from './chat'
import authReducer from './auth'
import usersReducer from './users'

const rootReducer = combineReducers({
    chat: chatReducer,
    auth: authReducer,
    users: usersReducer
})

export type RootState = ReturnType<typeof rootReducer>
export default rootReducer
