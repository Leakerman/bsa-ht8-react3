import { all, put, call, takeEvery } from "redux-saga/effects";
import axios from "axios";
import {
  fetchMessagesStart,
  fetchMessagesError,
  fetchMessagesSuccess,
} from "../actions/chat";
import {
  sortMessagesByDate,
  getUsersNumber,
  getMessageTime,
} from "../../shared/utils";
import {
  CHAT_DELETE_MESSAGE,
  CHAT_SEND_MESSAGE,
  CHAT_LIKE_MESSAGE,
  CHAT_EDIT_MESSAGE,
  FETCH_MESSAGES,
  DeleteMessageAction,
  LikeMessageAction,
  EditMessageAction,
  SendMessageAction,
} from "../../types/chat";
import config from "../../shared/config";

export function* fetchMessages() {
  try {
    yield put(fetchMessagesStart());
    const { data: messages } = yield call(
      axios.get,
      `${config.url}messages`
    );
    sortMessagesByDate(messages);

    const messagesAmount: number = messages.length;
    const participants = getUsersNumber(messages);

    const lastMessage = messages[messages.length - 1];
    const lastMessageDate = getMessageTime(lastMessage.createdAt);

    yield put(
      fetchMessagesSuccess({
        headerData: {
          chatName: "my chat",
          participants,
          messagesAmount,
          lastMessageDate,
        },
        pending: false,
        messages,
      })
    );
  } catch (error) {
    yield put(fetchMessagesError(error));
    console.error("fetchMessages error:", error.message);
  }
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* sendMessage(action: SendMessageAction) {
  try {
    yield call(
      axios.post,
      `${config.url}messages`,
      action.message
    );
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.error("sendMessages error:", error.message);
  }
}

function* watchSendMessage() {
  yield takeEvery(CHAT_SEND_MESSAGE, sendMessage);
}

export function* deleteMessage(action: DeleteMessageAction) {
  try {
    yield call(axios.delete, `${config.url}messages/${action.id}`);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.error("deleteMessages error:", error.message);
  }
}

function* watchDeleteMessage() {
  yield takeEvery(CHAT_DELETE_MESSAGE, deleteMessage);
}

export function* likeMessage(action: LikeMessageAction) {
  try {
    const { data: message } = yield call(
      axios.get,
      `${config.url}messages/${action.messageId}`
    );
    const likes: string[] = message.likes;

    if (!likes.includes(action.currentUserId)) {
      likes.push(action.currentUserId);
    } else {
      likes.splice(likes.indexOf(action.currentUserId), 1);
    }

    yield call(
      axios.put,
      `${config.url}messages/${action.messageId}`,
      { likes }
    );

    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.error("likeMessages error:", error.message);
  }
}

function* watchLikeMessage() {
  yield takeEvery(CHAT_LIKE_MESSAGE, likeMessage);
}

export function* editMessage(action: EditMessageAction) {
  try {
    yield call(
      axios.put,
      `${config.url}messages/${action.messageId}`,
      { text: action.text }
    );
  } catch (error) {
    console.error("editMessages error:", error.message);
  }
}

function* watchEditMessage() {
  yield takeEvery(CHAT_EDIT_MESSAGE, editMessage);
}

export default function* chatSagas() {
  yield all([
    watchFetchMessages(),
    watchSendMessage(),
    watchDeleteMessage(),
    watchLikeMessage(),
    watchEditMessage(),
  ]);
}
