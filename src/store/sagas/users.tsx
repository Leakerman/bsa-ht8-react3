import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import config from '../../shared/config';
import { FETCH_USERS, FETCH_USERS_SUCCESS, ADD_USER,AddUserAction, UpdateUserAction, UPDATE_USER, DeleteUserAction, DELETE_USER } from '../../types/users';

export function* fetchUsers() {
	try {
		const users = yield call(axios.get, `${config.url}users`);
		yield put({ type: FETCH_USERS_SUCCESS,  users: users.data  })
	} catch (error) {
		console.error('fetchUsers error:', error.message)
	}
}

function* watchFetchUsers() {
	yield takeEvery(FETCH_USERS, fetchUsers)
}

export function* addUser(action: AddUserAction) {
	try {
		yield call(axios.post, `${config.url}users`, action.user);
		yield put({ type: FETCH_USERS });
	} catch (error) {
		console.log('createUser error:', error.message);
	}
}

function* watchAddUser() {
	yield takeEvery(ADD_USER, addUser)
}

export function* updateUser(action: UpdateUserAction) {
	try {
		yield call(axios.put, `${config.url}users/${action.userId}`, action.updated);
		yield put({ type: FETCH_USERS });
	} catch (error) {
		console.log('updateUser error:', error.message);
	}
}

function* watchUpdateUser() {
	yield takeEvery(UPDATE_USER, updateUser)
}

export function* deleteUser(action: DeleteUserAction) {
	try {
		yield call(axios.delete, `${config.url}users/${action.userId}`);
		yield put({ type: FETCH_USERS })
	} catch (error) {
		console.log('deleteUser Error:', error.message);
	}
}

function* watchDeleteUser() {
	yield takeEvery(DELETE_USER, deleteUser)
}

export default function* usersSagas() {
	yield all([
		watchFetchUsers(),
		watchAddUser(),
		watchUpdateUser(),
		watchDeleteUser()
	])
};