import { AUTH_LOGIN, AuthActionTypes } from "../../types/auth";
import { IUser } from "../../types/interfaces";

export function login(user: IUser, isAdmin: boolean): AuthActionTypes {
  return {
    type: AUTH_LOGIN,
    user,
    isAdmin,
  };
}
