import {
  CHAT_SET_EDITING_MESSAGE_TEXT,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_START,
  FETCH_MESSAGES_ERROR,
  CHAT_DELETE_MESSAGE,
  CHAT_SEND_MESSAGE,
  CHAT_LIKE_MESSAGE,
  CHAT_EDIT_MESSAGE,
  FETCH_MESSAGES,
  ChatActionTypes,
} from "../../types/chat";
import { IMessage } from "../../types/interfaces";

export function sendMessage(message: IMessage): ChatActionTypes {
  return {
    type: CHAT_SEND_MESSAGE,
    message,
  };
}

export function editMessage(messageId: string, text: string): ChatActionTypes {
  return {
    type: CHAT_EDIT_MESSAGE,
    messageId,
    text
  };
}

export function setEditingMessageText(text: string): ChatActionTypes {
  return {
    type: CHAT_SET_EDITING_MESSAGE_TEXT,
    text,
  };
}

export function fetchMessages(): ChatActionTypes {
  return {
    type: FETCH_MESSAGES,
  };
}

export function deleteMessage(id: string): ChatActionTypes {
  return {
    type: CHAT_DELETE_MESSAGE,
    id
  };
}

export function likeMessage(messageId: string, currentUserId: string): ChatActionTypes {
  return {
    type: CHAT_LIKE_MESSAGE,
    messageId,
    currentUserId
  };
}

export function fetchMessagesSuccess(payload: any): ChatActionTypes {
  return {
    type: FETCH_MESSAGES_SUCCESS,
    payload,
  };
}

export function fetchMessagesError(error: string): ChatActionTypes {
  return {
    type: FETCH_MESSAGES_ERROR,
    error,
  };
}

export function fetchMessagesStart(): ChatActionTypes {
  return {
    type: FETCH_MESSAGES_START,
  };
}
