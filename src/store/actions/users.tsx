import { IUser } from "../../types/interfaces";
import {
  UsersActionTypes,
  ADD_USER,
  UPDATE_USER,
  DELETE_USER,
  FETCH_USERS,
  SET_EDITING_USER,
} from "../../types/users";

export function addUser(user: IUser): UsersActionTypes {
  return {
    type: ADD_USER,
    user,
  };
}

export function updateUser(userId: string, updated: any): UsersActionTypes {
  return {
    type: UPDATE_USER,
    userId,
    updated,
  };
}

export function deleteUser(userId: string): UsersActionTypes {
  return {
    type: DELETE_USER,
    userId,
  };
}

export function setEditingUser(user: IUser): UsersActionTypes {
  return {
    type: SET_EDITING_USER,
    user,
  };
}

export function fetchUsers() {
  return {
    type: FETCH_USERS,
  };
}
