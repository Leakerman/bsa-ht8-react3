import React from "react";
import "./ChatHeader.scss";
import { RootState } from "../../store/reducers/rootReducer";
import { connect, ConnectedProps } from "react-redux";

function mapStateToProps(state: RootState) {
  return {
    headerData: state.chat.headerData,
  };
}

const connector = connect(mapStateToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

function ChatHeader(props: PropsFromRedux) {
  const {
    chatName,
    participants,
    messagesAmount,
    lastMessageDate,
  } = props.headerData;

  return (
    <div className="chat__header">
      <div className="chat__header-info">
        <span className="chat-name">{chatName}</span>
        <span className="chat-participants">{participants} participants</span>
        <span className="chat-messages">{messagesAmount} messages</span>
      </div>
      <div className="chat__last-message-date">
        last message at {lastMessageDate}
      </div>
    </div>
  );
}

export default connector(ChatHeader);
