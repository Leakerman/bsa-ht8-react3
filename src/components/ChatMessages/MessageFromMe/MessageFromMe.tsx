import React from "react";
import { connect, ConnectedProps } from "react-redux";
import { useHistory } from "react-router-dom";
import { deleteMessage, setEditingMessageText } from "../../../store/actions/chat";
import "./MessageFromMe.scss";

function mapDispatchToProps(dispatch: any) {
  return {
    deleteMessage: (id: string) => dispatch(deleteMessage(id)),
    setEditingMessageText: (text: string) => dispatch(setEditingMessageText(text)),
  };
}

const connector = connect(null, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

type MessageFromMeProps = PropsFromRedux & {
  messageId: string;
  text: string;
  likesAmount: number;
  date: string;
};

function MessageFromMe(props: MessageFromMeProps) {
  const history = useHistory();
  return (
    <div className="message from-me">
      <div className="message__text">{props.text}</div>
      <div className="message__footer">
        <div className="like">
          <span className="material-icons icon-like">favorite</span>
          {props.likesAmount}
        </div>
        <span
          onClick={() => props.deleteMessage(props.messageId)}
          className="material-icons icon-delete"
        >
          delete
        </span>
        <span
          onClick={() => {
            props.setEditingMessageText(props.text)
            history.push(`/message/edit/${props.messageId}`);
          }}
          className="material-icons icon-edit"
        >
          edit
        </span>
        <div className="date">{props.date}</div>
      </div>
    </div>
  );
}

export default connector(MessageFromMe);
