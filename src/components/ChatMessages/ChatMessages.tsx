import React, { Fragment, PureComponent } from "react";
import { connect } from "react-redux";
import DayDelimiter from "./DayDelimiter/DayDelimiter";
import { getMessagesDate, createMessage } from "../../shared/utils";
import { RootState } from "../../store/reducers/rootReducer";
import { IMessage } from "../../types/interfaces";
import "./ChatMessages.scss";

function mapStateToProps(state: RootState) {
  return {
    currentUser: state.auth.currentUser,
    messages: state.chat.messages,
  };
}

const connector = connect(mapStateToProps);

class ChatMessages extends PureComponent<any> {
  todayMessagesDate: string;
  constructor(props: any) {
    super(props);
    this.todayMessagesDate = "";
  }

  componentDidMount() {
    const chatBox = document.querySelector(".chat__messages-box");
    chatBox!.scrollTop = chatBox!.scrollHeight;
  }

  render() {
    return (
      <div className="chat__messages-box">
        {this.props.messages.map((message: IMessage) => {
          const messageDate = getMessagesDate(message.createdAt!);
          let delimiter = null;
          if (this.todayMessagesDate !== messageDate) {
            const delimiterDate = new Date(message.createdAt!).toLocaleString(
              "en-US",
              {
                month: "long",
                day: "numeric",
              }
            );
            this.todayMessagesDate = messageDate;
            delimiter = <DayDelimiter date={delimiterDate} />;
          }

          return (
            <Fragment key={message.id! + Date.now()}>
              {delimiter}
              {createMessage(
                message,
                this.props.currentUser.id,
              )}
            </Fragment>
          );
        })}
      </div>
    );
  }
}

export default connector(ChatMessages);
