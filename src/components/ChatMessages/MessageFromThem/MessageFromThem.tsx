import React from "react";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../../store/reducers/rootReducer";
import { likeMessage } from "../../../store/actions/chat";
import "./MessageFromThem.scss";

function mapDispatchToProps(dispatch: any) {
  return {
    likeMessage: (messageId: string, currentUserId: string) =>
      dispatch(likeMessage(messageId, currentUserId)),
  };
}

function mapStateToProps(state: RootState) {
  return {
    currentUserId: state.auth.currentUser.id,
  };
}

const connector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

type MessageFromThemProps = PropsFromRedux & {
  messageId: string;
  avatar: string;
  text: string;
  isLiked: boolean;
  likesAmount: number;
  date: string;
};

function MessageFromThem(props: MessageFromThemProps) {
  return (
    <div className="message from-them">
      <div>
        <img
          id="avatar"
          src={props.avatar}
          onError={(e) => {
            const img = e.target as HTMLImageElement
            img.src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRX9GqqDvtUhZhJyfKO0Jvv_qxnk1YoXhDdeLBLMq_FzfdGOd2j&s"
          }}
          alt="avatar"
          className="message__avatar"
        />
        <div className="message__text">{props.text}</div>
      </div>
      <div className="message__footer">
        <div className="like">
          <span
            onClick={() =>
              props.likeMessage(props.messageId, props.currentUserId!)
            }
            className="material-icons icon-like"
          >
            {props.isLiked ? "favorite" : "favorite_border"}
          </span>
          {props.likesAmount}
        </div>
        <div className="date">{props.date}</div>
      </div>
    </div>
  );
}

export default connector(MessageFromThem);
