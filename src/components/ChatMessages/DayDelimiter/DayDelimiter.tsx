import React from "react";
import PropTypes from "prop-types";
import "./DayDelimiter.scss";

interface IDayDelimiterProps {
  date: string;
}

function DayDelimiter(props: IDayDelimiterProps) {
  return <div className="chat__day-delimiter">{props.date}</div>;
}

DayDelimiter.propTypes = {
  date: PropTypes.string,
};

export default DayDelimiter;
