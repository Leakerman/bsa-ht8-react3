import { Route, Switch, Redirect, useHistory } from "react-router-dom";
import React, { Fragment } from "react";
import { connect } from "react-redux";
import { RootState } from "./store/reducers/rootReducer";
import { IRoute } from "./types/interfaces";

import MessageEdit from "./containers/MessageEdit/MessageEdit";
import Login from "./containers/Login/Login";
import Users from "./containers/Users/Users";
import Chat from "./containers/Chat/Chat";
import Layout from "./hoc/Layout/Layout";
import UserEditor from "./containers/UserEditor/UserEditor";

function mapStateToProps(state: RootState) {
  return {
    isAdmin: state.auth.isAdmin,
    isAuthenticated: state.auth.isAuthenticated,
  };
}

const connector = connect(mapStateToProps);

function App(props: any) {
  const history = useHistory();
  let routes = <Route path="/login" component={Login} />;
  let links: IRoute[] = [{ path: "/login", component: Login, name: "login" }];

  if (props.isAuthenticated) {
    if (props.isAdmin) {
      alert("Hello, you entered as an admin!");
      routes = (
        <Fragment>
          <Route path="/login" component={Login} />
          <Route exact path="/users" component={Users} />
          <Route exact path="/users/edit" component={UserEditor} />
          <Route path="/users/edit/:id" component={UserEditor} />
          <Route path="/chat" component={Chat} />
          <Route path="/message/edit/:id" component={MessageEdit} />
        </Fragment>
      );
      links = links.concat(
        { path: "/users", component: Users, name: "users" },
        { path: "/chat", component: Chat, name: "chat" }
      );
      history.push("/users");
    } else {
      alert("Hello, you entered as a regular user!");
      routes = (
        <Fragment>
          <Route path="/login" component={Login} />
          <Route path="/chat" component={Chat} />
          <Route path="/message/edit/:id" component={MessageEdit} />
        </Fragment>
      );
      links = links.concat([
        { path: "/chat", component: Chat, name: "chat" },
      ]);
      history.push("/chat");
    }
  }

  return (
    <Layout routes={links}>
      <Switch>
        {routes}
        <Redirect to="/login" />
      </Switch>
    </Layout>
  );
}

export default connector(App);
