import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import { IRoute } from "../../types/interfaces";
import "./Layout.scss";

type LayoutProps = {
  routes: IRoute[];
};

export default class Layout extends Component<LayoutProps> {
  render() {
    return (
      <Fragment>
        <header className="header">
          Logo
          {this.props.routes.map((route) => {
            if (!route.name) return null;
            return (
              <NavLink key={route.path} className="nav-link" to={route.path}>
                {route.name}
              </NavLink>
            );
          })}
        </header>
        <main className="main">{this.props.children}</main>
        <footer className="footer">
          <div className="copyright">Copyright</div>
        </footer>
      </Fragment>
    );
  }
}
