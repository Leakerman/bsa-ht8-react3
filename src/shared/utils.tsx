import { IMessage } from "../types/interfaces";
import MessageFromMe from "../components/ChatMessages/MessageFromMe/MessageFromMe";
import React from "react";
import MessageFromThem from "../components/ChatMessages/MessageFromThem/MessageFromThem";

export function getMessageTime(date: string): string {
  const time = new Date(date);
  return `${time.getHours()}:${time.getMinutes()}`;
}

export function getMessagesDate(date: string): string {
  return new Date(date).toLocaleString("en-US", {
    year: "numeric",
    month: "numeric",
    day: "numeric",
  });
}

export function createMessage(
  message: IMessage,
  currentUserId: string,
) {
  const creationTime = getMessageTime(message.createdAt!);
  const likesAmount = message.likes!.length;
  const { text, id } = message;
  if (message.userId === currentUserId) {
    return (
      <MessageFromMe
        key={text + Date.now()}
        messageId={id!}
        text={text}
        likesAmount={likesAmount}
        date={creationTime}
      />
    );
  } else {
    const isLiked = message.likes.includes(currentUserId);
    const { avatar } = message;

    return (
      <MessageFromThem
        messageId={id!}
        text={text}
        likesAmount={likesAmount}
        isLiked={isLiked}
        avatar={avatar}
        date={creationTime}
      />
    );
  }
}

export function sortMessagesByDate(messages: IMessage[]): void {
  messages.sort((a: IMessage, b: IMessage) => {
    const dateB = (new Date(b.createdAt!) as unknown) as number;
    const dateA = (new Date(a.createdAt!) as unknown) as number;
    return dateA - dateB;
  });
}

export function getUsersNumber(messages: IMessage[]): number {
  const users: string[] = [];
  messages.forEach((message: IMessage) => {
    if (!users.includes(message.userId)) users.push(message.userId);
  });
  return users.length;
}
