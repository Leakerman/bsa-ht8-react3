import { IMessage } from "./interfaces";

export const FETCH_MESSAGES = "FETCH_MESSAGES";
export const FETCH_MESSAGES_START = "FETCH_MESSAGES_START";
export const FETCH_MESSAGES_ERROR = "FETCH_MESSAGES_ERROR";
export const FETCH_MESSAGES_SUCCESS = "FETCH_MESSAGES_SUCCESS";
export const CHAT_SEND_MESSAGE = "CHAT_SEND_MESSAGE";
export const CHAT_LIKE_MESSAGE = "CHAT_LIKE_MESSAGE";
export const CHAT_EDIT_MESSAGE = "CHAT_EDIT_MESSAGE";
export const CHAT_DELETE_MESSAGE = "CHAT_DELETE_MESSAGE";
export const CHAT_SET_EDITING_MESSAGE_TEXT = "CHAT_SET_EDITING_MESSAGE_TEXT";

interface FetchMessagesAction {
  type: typeof FETCH_MESSAGES;
}

interface FetchMessagesStartAction {
  type: typeof FETCH_MESSAGES_ERROR;
  error: string;
}

interface FetchMessagesSuccessAction {
  type: typeof FETCH_MESSAGES_SUCCESS;
  payload: any;
}

interface FetchMessagesErrorAction {
  type: typeof FETCH_MESSAGES_START;
}

interface SetEditingMessageTextAction {
  type: typeof CHAT_SET_EDITING_MESSAGE_TEXT;
  text: string;
}

export interface EditMessageAction {
  type: typeof CHAT_EDIT_MESSAGE;
  messageId: string;
  text: string;
}

export interface LikeMessageAction {
  type: typeof CHAT_LIKE_MESSAGE;
  messageId: string;
  currentUserId: string;
}

export interface SendMessageAction {
  type: typeof CHAT_SEND_MESSAGE;
  message: IMessage;
}

export interface DeleteMessageAction {
  type: typeof CHAT_DELETE_MESSAGE;
  id: string;
}

export type ChatActionTypes =
  | FetchMessagesAction
  | FetchMessagesStartAction
  | FetchMessagesSuccessAction
  | FetchMessagesErrorAction
  | SetEditingMessageTextAction
  | EditMessageAction
  | SendMessageAction
  | LikeMessageAction
  | DeleteMessageAction;
