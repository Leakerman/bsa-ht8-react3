import { IUser } from "./interfaces";

export const AUTH_LOGIN = "AUTH_LOGIN";

interface AuthLoginAction {
  type: typeof AUTH_LOGIN;
  user: IUser;
  isAdmin: boolean;
}

export type AuthActionTypes = AuthLoginAction;
