export interface IMessage {
  id?: string;
  text: string;
  user: string;
  avatar: string;
  userId: string;
  editedAt?: string;
  createdAt?: string;
  likes: string[];
}

export interface IUser {
  id?: string;
  login: string;
  avatar: string;
  password?: string
}

export interface IRoute {
  path: string;
  component: any;
  name: string;
}

export interface HeaderData {
  chatName?: string;
  participants?: number;
  messagesAmount?: number;
  lastMessageDate?: string;
}

export interface ChatState {
  headerData: HeaderData;
  pending: boolean;
  messages: IMessage[];
  messageInputValue: string;
  error: string;
  editingMessageText: string;
}

export interface ModalState {
  isOpen: boolean;
  text: string;
  editingMessageId: string;
}

export interface AuthState {
  currentUser: IUser;
  isAdmin: boolean;
  isAuthenticated: boolean;
}

export interface usersState {
  users: IUser[],
  editingUser: IUser
}