import { IUser } from "./interfaces";

export const FETCH_USERS = "FETCH_USERS";
export const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";
export const ADD_USER = "ADD_USER";
export const UPDATE_USER = "UPDATE_USER";
export const DELETE_USER = "DELETE_USER";
export const SET_EDITING_USER = "SET_EDITING_USER";

interface FetchUsersAction {
  type: typeof FETCH_USERS;
}

interface FetchUsersSuccessAction {
  type: typeof FETCH_USERS_SUCCESS;
  users: IUser[];
}

interface SetEditingUserAction {
  type: typeof SET_EDITING_USER,
  user: IUser
}

export interface AddUserAction {
  type: typeof ADD_USER;
  user: IUser;
}

export interface UpdateUserAction {
  type: typeof UPDATE_USER;
  userId: string;
  updated: any;
}

export interface DeleteUserAction {
  type: typeof DELETE_USER;
  userId: string;
}

export type UsersActionTypes =
  | FetchUsersAction
  | FetchUsersSuccessAction
  | AddUserAction
  | UpdateUserAction
  | DeleteUserAction
  | SetEditingUserAction
